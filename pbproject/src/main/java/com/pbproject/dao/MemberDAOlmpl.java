package com.pbproject.dao;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.pbproject.vo.MemberVO;

@Repository
public class MemberDAOlmpl implements MemberDAO {

	@Inject SqlSession sql;
	
	private static String namespace = "com.pbproject.mappers.member";
	
	@Override
	public void register(MemberVO vo) throws Exception {
		sql.insert(namespace + ".register", vo);
	}

	@Override
	public MemberVO login(MemberVO vo) throws Exception {
		return sql.selectOne(namespace + ".login", vo);
	}

}