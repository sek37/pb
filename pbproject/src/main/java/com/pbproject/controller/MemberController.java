package com.pbproject.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.pbproject.service.MemberService;
import com.pbproject.vo.MemberVO;

@Controller
@RequestMapping("/member/*")
public class MemberController {
	//로그확인
	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
	
	@Inject
	MemberService service;
	
	// 회원가입  화면
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public void getRegister() throws Exception {
		logger.info("get register");
	}
	
	//회원가입
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String postmemberRegister(MemberVO vo) throws Exception{
		
		logger.info("Post register");
		
		service.register(vo);
		
		return "redirect:/";
	}
	
	//로그인 화면
	@RequestMapping("/login")
	public void getLogin() throws Exception{
		logger.info("get login");
	}
	
	//로그인
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(MemberVO vo, HttpServletRequest req, RedirectAttributes rttr) throws Exception{
		logger.info("Post MemberLogin");
		
		HttpSession session = req.getSession();
		MemberVO login = service.login(vo);

		if(login == null) {
			session.setAttribute("member", null);
			rttr.addFlashAttribute("msg", false);
		} else {
			session.setAttribute("member", login);
		}
		return "redirect:/member/loginsuccess";
	}
	
	//로그아웃
	@RequestMapping(value ="/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) throws Exception{
		logger.info("get Logout");
		
		session.invalidate();
		
		return "redirect:/";
	}
	
	//로그인 성공 화면
	@RequestMapping("/loginsuccess")
	public String loginsuccess() {
		return "/member/loginsuccess";
	}
}